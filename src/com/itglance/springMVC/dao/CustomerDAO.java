package com.itglance.springMVC.dao;

import com.itglance.springMVC.entity.Customer;

import java.sql.SQLException;
import java.util.List;

public interface CustomerDAO {

    List<Customer> getAll() throws ClassNotFoundException, SQLException;

}
